'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.addColumn(
      'tbl_cars', //Nama Tabel Tujuan yang mau di tambahkan
      'url_image', //Nama Kolom yang mau ditambahkan
      {
        type : Sequelize.STRING,
        allowNull : false,
      } 
    )
  },


  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
