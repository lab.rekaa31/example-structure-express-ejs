'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     await queryInterface.bulkInsert('tbl_cars', [
			{ 
        name : "Mobil 1",
        harga : 2000000,
        url_image : "https://res.cloudinary.com/polbanjuara21/image/upload/v1650616878/binar-asset-cars/mobil-1.jpg",
        id_size: 3,
				createdAt: new Date(),
				updatedAt: new Date()
			},
      {
        name: "Avanza",
        harga: 233000,
        url_image: "https://www.toyota.astra.co.id/sites/default/files/2021-11/image1.jpg",
        createdAt: new Date(),
        updatedAt: new Date(),
        id_size: 4
      },
      {
        name: "Voxy",
        harga: 560000,
        url_image: "https://www.toyota.astra.co.id/sites/default/files/2022-02/New%20Resilient%20Front%20Bumper%20and%20Intriguing%20Grille%20Design.jpg",
        createdAt: new Date(),
        updatedAt: new Date(),
        id_size: 3
      },
      {
        name: "Calya",
        harga: 151000,
        url_image: "https://www.toyota.astra.co.id/sites/default/files/2021-08/calya%20side%20view.jpeg",
        createdAt: new Date(),
        updatedAt: new Date(),
        id_size: 5
      },
      {
        name: "Sienta",
        harga: 312000,
        url_image: "https://www.toyota.astra.co.id/sites/default/files/2021-08/sienta%20yellow.jpeg",
        createdAt: new Date(),
        updatedAt: new Date(),
        id_size: 4
      }
		], {});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
