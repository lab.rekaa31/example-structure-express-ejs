const db = require('./../../models')

const tbl_cars = db.tbl_cars

async function getAllCars(req, res) {

    try {
        // Get From Table Database with Model tbl_cars
        let cars = await tbl_cars.findAll()
        
        // Send Response
        res.status(200).json({
            message:"success",
            data : cars
        })

    } catch (error) {
        console.log(error)
    }

}

async function newCars(req, res) {

    try {

        let carReq = req.body

        // Penyesuaian Struktur Data
        let car = {
            name:carReq.name,
            harga:carReq.harga,
            url_image:carReq.url_image,
            id_size:carReq.id_size,
            createdAt:new Date(),
            updatedAt:new Date(),
        }

        tbl_cars.create(car).then(() => {
            res.status(200).json({
                message:"success",
                data : car
            })
        }).catch((error) => {
            console.log(error)
        })

    } catch (error) {
        console.log(error)
    }
    
}

async function showCar(req, res){
    try {
        let idCar = Number(req.params.id)
        let car = await tbl_cars.findOne({
            where : {id:idCar}
        })

        res.status(200).json({
            message:"success",
            data : car
        })
    } catch (error) {
        console.log(error)
    }
}

async function editCars(req, res) {
    try {
        let carReq = req.body
        let idCar = Number(req.params.id)

        // Penyesuaian Struktur Data
        let car = {
            name:carReq.name,
            harga:carReq.harga,
            url_image:carReq.url_image,
            id_size:Number(carReq.id_size),
            createdAt:new Date(),
            updatedAt:new Date(),
        }

        tbl_cars.update(car, {
            where : {id:idCar}
        }).then(() => {
            res.status(200).json({
                message:"success",
                data : []
            })
        }).catch((error) => {
            console.log(error)
        })

       
    } catch (error) {
        console.log(error)
    }
}

function deleteCar(req, res){
    try {

        let idCar = req.params.id

        tbl_cars.destroy({where : {id:idCar}}).then(() => {
            res.status(200).json({
                message:"success",
                data : []
            })
        }).catch((error) =>{
            console.log(error)
        })

        
    } catch (error) {
        console.log(error)
    }
}

module.exports = {
    getAllCars,
    newCars,
    editCars,
    showCar, 
    deleteCar
}