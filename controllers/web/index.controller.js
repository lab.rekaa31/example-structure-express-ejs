const { default: axios } = require("axios")

function index(req, res) {

    axios.get("http://localhost:8081/api/cars").then(
        (response) => res.render('../views/index', {listCars:response.data.data})
    )

}

module.exports = {
    index
}