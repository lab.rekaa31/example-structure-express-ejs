const express = require('express')

// Define Object Express
const app = express()

// Import Router
const webRouter = require('./../routes/web.router')
const apiRouter = require('./../routes/api.router')
const path = require('path')

// Use Express JSON
app.use(express.json())

// Use Express URL Encode
app.use(express.urlencoded())

// Define File Static
app.use(express.static(path.join(__dirname, '../views/assets/')));

// Set EJS Engine
app.set('view engine', 'ejs')

// Define Port
const PORT = 8081

// Define Router Web
app.use('/', webRouter)
app.use('/api', apiRouter)

// Define Server
app.listen(PORT, () => {
    console.log("Server is Running at Port : "+PORT)
})